<?php
namespace app\rbac;

use app\models\User;
use app\models\Activity;

use yii\rbac\Rule;
use Yii; 

class OwnUserRule extends Rule
{
	public $name = 'ownUserRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['user']) ? $params['user']->id == (User::findIdentity($user)->id) : false;
			
		}
		return false;
	}
}